﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CarGeneticAlgorithm
{
	public List<CarDNA> Population { get; set; }
	public List<CarDNA> ListForParents { get; set; }
	public int Generation { get; set; }
	public float BestFitness { get; set; }
	public CarDNA BestGenes { get; set; }

	public int Elitism;
	public float MutationRate;
	private int dnaSize;
	private float fitnessSum;
	private List<CarDNA> newPopulation;
	[SerializeField] CurrentPopulationObject CurrentPopulationObject;

	private List<Tuple<Vector3, Vector3, Vector3>> listOfCarDimensions;
	private List<float> distances;
	private RunScript RunScript;
	
	public CarGeneticAlgorithm(int Elitism, float MutationRate, 
								List<Tuple<Vector3, Vector3, Vector3>> listOfCarDimensions, 
								List<float> distances, GameObject runScriptObject, 
									CurrentPopulationObject CurrentPopulationObject)
    {
		RunScript = runScriptObject.GetComponent<RunScript>();
		Generation = 0;
		this.Elitism = Elitism;
		this.MutationRate = MutationRate;
		this.listOfCarDimensions = listOfCarDimensions;
		this.distances = distances;
		this.dnaSize = listOfCarDimensions.Count;
		Population = new List<CarDNA>(dnaSize);
		newPopulation = new List<CarDNA>(dnaSize);
		
		this.CurrentPopulationObject = CurrentPopulationObject;

		for (int i = 0; i < dnaSize; i++)
        {
			Population.Add(new CarDNA(distances[i], listOfCarDimensions[i], shouldInitGenes: true));
		}
		//Debug.Log($"Population size: {Population.Count}");

	}

	public void NewGeneration(int numNewDNA = 0, bool crossoverNewDNA = false)
	{
		ListForParents = Population;
		int finalCount = Population.Count + numNewDNA;
		if (finalCount <= 0)
		{
			return;
		}
		if (Population.Count > 0)
		{
			CalculateFitness();
			Population.Sort(CompareDNA);

		}
		
		newPopulation.Clear();

		for (int i = 0; i < Population.Count; i++)
		{
			//Debug.Log($"looping {Population.Count} times");
			if (i < Elitism && i < Population.Count)
			{
				Population[i].Mutate(MutationRate);
				newPopulation.Add(Population[i]);
				//Debug.Log($"Adding Elite car: {Population[i]}");
			}
			else if (i < Population.Count || crossoverNewDNA)
			{
				CarDNA parent1 = ChooseParent();
				//Debug.Log("parent 1 fitness: "+parent1.Fitness);
				CarDNA parent2 = ChooseParent();
				//Debug.Log("parent 2 fitness: " + parent2.Fitness);

				//Debug.Log($"Parent 1 genes count: {parent1.Genes.Count}");

				CarDNA child = parent1.Crossover(parent2);
				child.Mutate(MutationRate);
				//Debug.Log($"Adding child: {child}");
				newPopulation.Add(child);
			}
			else
			{
				//Debug.Log("Adding rest of population");
				CarDNA newCar = new CarDNA(distances[i], listOfCarDimensions[i]);
				newCar.Mutate(MutationRate);
				newPopulation.Add(new CarDNA(distances[i], listOfCarDimensions[i]));
			}
		}
		//New population variable becomes old population, and population variable becomes new one
		List<CarDNA> tmpList = Population;
		Population = newPopulation;
		newPopulation = tmpList;

		//Gets the new populations dimensions and writes it to the data object
		GetNewPopulationDimensions();
		CurrentPopulationObject.listOfCarDimensions = listOfCarDimensions;


		//Debug.Log("new gen population: "+Population.Count);
		Generation++;
		//get the new fitness (the distances of the population) i hope :)))
		RunScript.enabled = true;
	}

	private void GetNewPopulationDimensions()
    {
		List<Tuple<Vector3, Vector3, Vector3>> tempList = new List<Tuple<Vector3, Vector3, Vector3>>();
		foreach (CarDNA carDNA in Population)
        {
			tempList.Add(carDNA.carDimensions);
        }
		listOfCarDimensions = tempList;
    }

	private int CompareDNA(CarDNA a, CarDNA b)
    {
		if (a.Fitness > b.Fitness)
		{
			return -1;
		}
		else if (a.Fitness < b.Fitness)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	private void CalculateFitness()
	{
		fitnessSum = 0;
		CarDNA best = Population[0];
		
		for (int i = 0; i < Population.Count; i++)
		{
			fitnessSum += Population[i].Fitness;
			//Debug.Log($"fitness of car: {Population[i].Fitness}");

			if (Population[i].Fitness > best.Fitness)
			{
				best = Population[i];
			}
		}
		//Debug.Log($"Best fitness of car: {best.Fitness}");

		BestFitness = best.Fitness;
		BestGenes = best;
	}

	private CarDNA ChooseParent()
    {
		int randomNumber = UnityEngine.Random.Range(0, ListForParents.Count);
		for (int i = 0; i < ListForParents.Count; i++)
		{
			/*
			if (randomNumber < Population[i].Fitness)
			{
				return Population[i];
			}
			randomNumber -= Population[i].Fitness;
			*/
			if (i == randomNumber)
            {
				var Parent = ListForParents[i];
				ListForParents.Remove(Parent);
				return Parent;
            }
		}

		return null;
	}




}
