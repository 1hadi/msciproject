using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SetUpScript : MonoBehaviour
{
    public GameObject car;
    public GameObject endWall;
    public  List<GameObject> listOfCars;
    [SerializeField] CurrentPopulationObject CurrentPopulationObject;
    public List<Tuple<Vector3, Vector3, Vector3>> listOfCarDimensions;
    public  List<float> distances;
    public float speed = 20;
    public int population = 5;
   
    private ChangeSize ChangeSize = new ChangeSize();
    private bool updateOn = true;

    void Awake()
    {
        //DontDestroyOnLoad()
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(UpdateOff());
        endWall = GameObject.Find("endWall");
        listOfCars = new List<GameObject>();
        distances = new List<float>();
        listOfCarDimensions = new List<Tuple<Vector3 , Vector3, Vector3>>();
        CreateCars(population);     
    }

    void CalculateDistance()
    {
        int i = 0;
        foreach(GameObject car in listOfCars)
        {
            GameObject carBody = car.transform.GetChild(2).gameObject;
            var distanceTravelled = 24.415f - Vector3.Distance(carBody.transform.position, endWall.transform.position);
            distanceTravelled = distanceTravelled < 0 ? 0 : distanceTravelled; 
            distances.Add(distanceTravelled);
            i++;
        }
    }
   
    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (updateOn)
        {
            foreach (GameObject car in listOfCars)
            {
                Rigidbody2D frontTire = car.transform.GetChild(0).gameObject.GetComponent<Rigidbody2D>();
                Rigidbody2D backTire = car.transform.GetChild(1).gameObject.GetComponent<Rigidbody2D>();
                frontTire.AddTorque(2f * -0.5f, ForceMode2D.Force);
                frontTire.AddTorque(2f * -0.5f, ForceMode2D.Force);
            }
        }
    }

    IEnumerator UpdateOff()
    {
        yield return new WaitForSeconds(15.0f);
        CalculateDistance();
        DeconstructCars();
        CurrentPopulationObject.listOfCarDimensions = listOfCarDimensions;
        CurrentPopulationObject.distances = distances;
        this.enabled = false;
        updateOn = false;
    }

    void DeconstructCars()
    {
        foreach (GameObject cars in listOfCars)
        {
            /*
             * front, back, body
             */
            GameObject frontTire = cars.transform.GetChild(0).gameObject;
            GameObject backTire = cars.transform.GetChild(1).gameObject;
            GameObject carBody = cars.transform.GetChild(2).gameObject;
            var carDimensions = new Tuple<Vector3, Vector3, Vector3>(frontTire.transform.localScale, backTire.transform.localScale, carBody.transform.localScale);
            listOfCarDimensions.Add(carDimensions);
        }
    }

    public void DeleteShadowClones()
    {
        try
        {
            for(int i = 1; i < listOfCars.Count; i++)
            {
                Destroy(listOfCars[i]);
            }
        }
        catch (MissingReferenceException e)
        {

        }
    }

    public void CreateCars(int size)
    {
     
        for (int i = 0; i < size; i++)
        {
            GameObject newCar = Instantiate(car, Vector3.zero, Quaternion.identity) as GameObject;
            var scaledCar = ChangeSize.ScaleCar(newCar);
            listOfCars.Add(scaledCar);
        }

    }

}
