﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CarDNA
{
	public List<Vector3> Genes { get; set; }
	public float Fitness { get; set; }

	private System.Random random;
    private bool shouldInitGenes;
	public Tuple<Vector3, Vector3, Vector3> carDimensions;
	private ChangeSize ChangeSize = new ChangeSize();

	public CarDNA(float distanceTravelled, Tuple<Vector3, Vector3, Vector3> carDimensions, bool shouldInitGenes = true)
	{
		Genes = new List<Vector3>();
		this.random = new System.Random();
		this.Fitness = distanceTravelled;
		this.carDimensions = carDimensions; 
		if (shouldInitGenes)
		{
			Genes.Add(carDimensions.Item1);
			Genes.Add(carDimensions.Item2);
			Genes.Add(carDimensions.Item3);
		}
	}

    public CarDNA Crossover(CarDNA otherParent)
	{ 
		CarDNA child = new CarDNA(Fitness, carDimensions, shouldInitGenes: true);
		var cutoff = random.Next(0, Genes.Count-1);
		for (int i = 0; i < Genes.Count-1; i++)
		{
			if (i >= cutoff)
            {
				child.Genes[i] = otherParent.Genes[i];
            }
			//child.Genes[i] = random.NextDouble() < 0.5 ? child.Genes[i] : otherParent.Genes[i];
		}
		return child;
    }

	public void Mutate(float MutationRate)
	{
		for (int i = 0; i < Genes.Count; i++)
        {
			if (random.NextDouble() < MutationRate)
            {
				Genes[i] = ChangeSize.RandomVector();
            }
        }	
	}

	public float CalculateFitness()
    {
		return Fitness;
    }

    
}
