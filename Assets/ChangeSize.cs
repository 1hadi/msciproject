using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSize : MonoBehaviour
{
    //public GameObject car;
    public GameObject carBody;
    public GameObject backTire;
    public GameObject frontTire;
    /*
     * front, back, body
     */
   
    public GameObject ScaleCar(GameObject car)
    {
        //Can make simpler
        carBody = car.transform.GetChild(2).gameObject;
        backTire = car.transform.GetChild(1).gameObject;
        frontTire = car.transform.GetChild(0).gameObject;
        carBody = Scale(carBody);
        frontTire = Scale(frontTire);
        backTire = Scale(backTire);

        return car;
    }

    public  GameObject Scale(GameObject gameObject)
    {
        gameObject = ChangeColour(gameObject);
        //if the game object is the car body it will change it both x and y, tires are changed just x
        if (carBody == gameObject)
        {
            _ = UnityEngine.Random.Range(0.0f, 1f) < 0.5 ?
           gameObject.transform.localScale += new Vector3(UnityEngine.Random.Range(0.0f, 0.5f), UnityEngine.Random.Range(0.0f, 0.5f)) :
           gameObject.transform.localScale -= new Vector3(UnityEngine.Random.Range(0.0f, 0.5f), UnityEngine.Random.Range(0.0f, 0.5f));
        } else
        {
            var scaleXY = UnityEngine.Random.Range(0.0f, 0.5f);
            _ = UnityEngine.Random.Range(0f, 1f) < 0.5 ?
           gameObject.transform.localScale += new Vector3(scaleXY, scaleXY) :
           gameObject.transform.localScale -= new Vector3(scaleXY, scaleXY);
        }
        Vector3 scaledValues = gameObject.transform.localScale;
        return  gameObject;
    }

    public GameObject Create(GameObject rootCar, Tuple<Vector3, Vector3, Vector3> dimensions)
    {
        //front, back, body
        frontTire = rootCar.transform.GetChild(0).gameObject;
        backTire = rootCar.transform.GetChild(1).gameObject;
        carBody = rootCar.transform.GetChild(2).gameObject;


        frontTire = ChangeColour(frontTire);
        backTire = ChangeColour(backTire);
        carBody = ChangeColour(carBody);
        
        frontTire.transform.localScale = dimensions.Item1;
        backTire.transform.localScale = dimensions.Item2;
        carBody.transform.localScale = dimensions.Item3;

        return rootCar;

    }

    public GameObject ChangeColour(GameObject gameObject)
    {
        //Create a new cube primitive to set the color on
        Color newColour = new Color(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f)
                                                                ,UnityEngine.Random.Range(0f, 1f), 1.0f);

        //Get the Renderer component from the new cube
        var objectRenderer = gameObject.GetComponent<Renderer>();

        //Call SetColor using the shader property name "_Color" and setting the color to red
        objectRenderer.material.SetColor("_Color", newColour);
        return gameObject;
    }

    public Vector3 RandomVector()
    {
        return new Vector3(UnityEngine.Random.Range(0.0f, 0.5f), UnityEngine.Random.Range(0.0f, 0.5f));
    }
            
}
