using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TestCarSetUp : MonoBehaviour
{
    [SerializeField] CurrentPopulationObject CurrentPopulationObject;
    public GameObject initialPopulation;
    private SetUpScript setup;

    void Start()
    {
        StartCoroutine(UpdateOff());
        setup = initialPopulation.GetComponent<SetUpScript>();
        CurrentPopulationObject.distances.Clear();
        CurrentPopulationObject.currentRun = 0;
        setup.enabled = true;
    }

    IEnumerator UpdateOff()
    {
        yield return new WaitForSeconds(16.0f);
        Debug.Log("Set up complete!");
        SceneManager.LoadScene("RunningScene", LoadSceneMode.Single);
    }
    
}
