using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/CurrentPopulationObject")]
public class CurrentPopulationObject : ScriptableObject
{
    public List<Tuple<Vector3, Vector3, Vector3>> listOfCarDimensions { get; set; }
    public List<float> distances { get; set; }
    public int currentRun { get; set; } 
}
