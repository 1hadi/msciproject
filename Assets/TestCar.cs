using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TestCar : MonoBehaviour
{

    [SerializeField] CurrentPopulationObject CurrentPopulationObject;
    [SerializeField] int populationSize = 5; //isnt currently used - see SetUpScript to change
    [SerializeField] float MutationRate = 0.05f;
    [SerializeField] int Elitism = 1;
    [SerializeField] GameObject runScriptObject;
    [SerializeField] Text bestFitnessText;
    [SerializeField] Text numGenerations;


    public List<Tuple<Vector3, Vector3, Vector3>> listOfCarDimensions;
    //public List<GameObject> listOfCars;
    public List<float> distances;
    private CarGeneticAlgorithm ga;
    private System.Random random;
    private ChangeSize ChangeSize = new ChangeSize();
    
    

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TurnOn());
        random = new System.Random();
        distances = CurrentPopulationObject.distances;
        listOfCarDimensions = CurrentPopulationObject.listOfCarDimensions;
        ga = new CarGeneticAlgorithm(Elitism, MutationRate, listOfCarDimensions, distances, runScriptObject, CurrentPopulationObject);
        ga.NewGeneration();
        
    }
    IEnumerator TurnOn()
    {
        yield return new WaitForSeconds(16.0f);
        CurrentPopulationObject.currentRun++;
        
        Debug.Log($"Current Generation: {CurrentPopulationObject.currentRun}");
        if (ga.BestFitness >= 20)
        {
            this.enabled = false;
        }
        SceneManager.LoadScene("RunningScene", LoadSceneMode.Single);
    }

    void Update()
    {
        bestFitnessText.text = ga.BestFitness.ToString();
        numGenerations.text = CurrentPopulationObject.currentRun.ToString();
    }

    



}
