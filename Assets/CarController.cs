using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    public Rigidbody2D backTire;
    public Rigidbody2D frontTire;
    public float torque;
    public float turn;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Update()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        frontTire.AddTorque(torque * turn, ForceMode2D.Force);
        frontTire.AddTorque(torque * turn, ForceMode2D.Force);

    }
}
