using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunScript : MonoBehaviour
{

    public List<Tuple<Vector3, Vector3, Vector3>> listOfCarDimensions;
    [SerializeField] GameObject rootCar;
    [SerializeField] CurrentPopulationObject CurrentPopulationObject;
    public List<GameObject> listOfCars;
    public List<float> distances;
    public GameObject endWall;
    private ChangeSize ChangeSize = new ChangeSize();


    public float speed = 20;
    private bool RunOn = true;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(RunOff());
        endWall = GameObject.Find("endWall");
        listOfCarDimensions = CurrentPopulationObject.listOfCarDimensions;
        listOfCars = new List<GameObject>();
        distances = new List<float>();
        CreateCars();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (RunOn)
        {
            foreach (GameObject car in listOfCars)
            {
                Rigidbody2D frontTire = car.transform.GetChild(0).gameObject.GetComponent<Rigidbody2D>();
                Rigidbody2D backTire = car.transform.GetChild(1).gameObject.GetComponent<Rigidbody2D>();
                frontTire.AddTorque(2f * -0.5f, ForceMode2D.Force);
                frontTire.AddTorque(2f * -0.5f, ForceMode2D.Force);
            }
        }
       
    }

    void CreateCars()
    {
        foreach(Tuple <Vector3, Vector3, Vector3> dimensions in listOfCarDimensions)
        {
            GameObject newCar = Instantiate(rootCar, Vector3.zero, Quaternion.identity) as GameObject;
            listOfCars.Add(ChangeSize.Create(newCar, dimensions));
        }
    }

    void CalculateDistance()
    {
        int i = 0;
        foreach (GameObject car in listOfCars)
        {
            GameObject carBody = car.transform.GetChild(2).gameObject;
            var distanceTravelled = 24.415f - Vector3.Distance(carBody.transform.position, endWall.transform.position);
            distanceTravelled = distanceTravelled < 0 ? 0 : distanceTravelled;
            distances.Add(distanceTravelled);
            i++;
        }
    }

    public void DeleteClones()
    {
        foreach(GameObject car in listOfCars)
        {
            Destroy(car);
        }
    }

    IEnumerator RunOff()
    {
        yield return new WaitForSeconds(15.0f);
        CalculateDistance();
        CurrentPopulationObject.listOfCarDimensions = listOfCarDimensions;
        CurrentPopulationObject.distances = distances;
        DeleteClones();
        this.enabled = false;
        RunOn = false;
    }
}
