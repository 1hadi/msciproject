# README #

### What is this repository for? ###
* Unity project for the evolution of the design of a Car 
* Version: 1.0

### How do I get set up? ###

* Summary of set up
Requires Unity Editor Version 2020.3.27f1 and any IDE for C# programming
* To run the project
Press play button on Unity editor, once program has been built. This will run the car game with the genetic algorithm
To run TestShakespeare, unselect TestCar SetUp under MainCamera object and enable the Canvas object and select the TestShakespeare script
* Troubleshooting
If program does not run
- check if current scene is SetUp Scene. This scene MUST run first 
- check if script Test Car SetUp under MainCamera game object is enabled

### Who do I talk to? ###

* Email: zfac144@live.rhul.ac.uk
